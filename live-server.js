const ls = require("live-server");
var params = {
	host: "0.0.0.0",
	root: "dist",
	open: false,
	file: "index.html",
	logLevel: 2
};

ls.start(params);
